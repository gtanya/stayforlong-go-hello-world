docker_repo = gtanya/stayforlong-go-hello-world


build:
	go build \
		-ldflags "-s -w" \
		-o out/helloWorld \
		./hello_world.go

docker-build:
	docker run -it --rm -v $(PWD):/app -w /app golang:1.13-buster make build


docker-build-container:
	docker build . -t $(docker_repo):latest

docker-push:
	docker push $(docker_repo):latest
