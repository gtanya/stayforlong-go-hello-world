package main

import (
	"fmt"
	"net/http"
	"os"
)

const (
	port = "80"
)

func HelloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello world")
	fmt.Fprintln(w,"Stack:", os.Getenv("Stack"))
    fmt.Fprintln(w,"Hostname:", os.Getenv("HOSTNAME"))
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "I'm running")
}

func main() {
	fmt.Printf("Starting server... at http://localhost:%v.\n", port)
	http.HandleFunc("/", HelloWorld)
	http.HandleFunc("/health", HealthCheck)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		fmt.Println("Error: " + err.Error())
	}
	fmt.Println("Program exit")
}
