FROM golang:latest

RUN mkdir /app
WORKDIR /app

COPY out/helloWorld .

EXPOSE 80

CMD ["/app/helloWorld"]