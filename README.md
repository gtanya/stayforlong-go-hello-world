# go-hello-world
Simple HTTP Hello World in Golang

# Build

### Requirements
- docker installed and running
- A DockerHub account with a repository. Default repository 
  `gtanya/stayforlong-go-hello-world`

### How to build
From the root of this repository run 
``` 
make docker-build
``` 
and the binary will be located in `./out/` directory (the binary will be
compiled for `linux amd64` platform only).

### How to build docker container

You need to build the GO app previously.

Configure your DockerHub repository in "Makefile" setting the value at
variable `docker_repo`


Execute:
```
make docker-build-container
```

### How to push docker container

You need to build the Docker container previously.

Login into your DockerHub account: 
```
docker login
```

Push the image to the remote server 
```
make docker-push
```



# Run
Just execute the binary and the webserver will be available at port `80`

### Endpoints

- `/` -> Hello World
- `/health` -> health check

### With docker

docker run --rm -d --name goapp -p 80:80 gtanya/stayforlong-go-hello-world